Yet Another Dialog -- GTK+ 2 Maintenance Branch
====================

This repository contains a feature-limited version of the "yad" program.

The code in this repository was split off from (what I deem to be) the
definitive "upstream" suited-to-gtk2 codebase maintianed by 'step'
 (one of the http://distro.ibiblio.org/fatdog/web/ FatDog64 distro maintainers)

https://github.com/step-/yad

Notable changes for this repository, vs upstream:

* the ancillary "show all stock gtk icons" program is removed

* this build disables: html, gio, spell, sourceview
  and omits from the manpage any mention of features related to these.
  The manpage also striveS to avoid presenting "if gtk3 THIS, else THAT",
  "only if gtk2, does not work for gtk3" (confusing disclaimers of
  conditional, and often contrary, explanations)

====================

Yad enables you to display GTK+ dialog boxes from the command line or
shell scripts. YAD depends on GTK+ only. The minimum GTK+ version is 2.16.0.

This software is licensed under the GPL v.3

This branch (maintain-gtk2) was forked from the upstream project at release
0.42 (d0021d0) to continue GTK+ 2 support for the Fatdog64 Linux distribution.
It should serve other minimal distributions that rely on GTK+ 2, and package yad.

In the spirit of a maintainance project, the focus is on fixing bugs.
New features are considered but rarely added.

Building the git version (the upstream, non-debian-packaged version) 
----------------------

Although the  https://github.com/step-/yad  branch can be built against
either of version 2 and version 3 of the GTK+ library, it is only
maintained and tested for GTK+ 2.

Get the latest source code with command:

```sh
git clone https://github.com/step-/yad.git yad_gtk2
```

Generate build scripts, configure and build the project:

```sh
cd yad &&
git checkout maintain-gtk2 &&
autoreconf -ivf &&
intltoolize &&
./configure &&
make &&
: install with: make install
```

To build successfully you may need to install the following packages:
* GNU Autotools (https://www.gnu.org/software/autoconf/ http://www.gnu.org/software/automake/)
* Intltool >= 0.40.0 (http://freedesktop.org/wiki/Software/intltool/)
* GTK+ >= 2.16.0 (http://www.gtk.org)
with appropriate *-dev* packages depending on your distro.

When you run `configure` you can pass some options to build yad with the the following libraries:
* Webkit - for supporting the HTML dialog option (http://webkitgtk.org)
* GtkSourceView - for syntax highlighting in text text-info dialog (https://wiki.gnome.org/Projects/GtkSourceView)
* GtkSpell3 - for spell checkinging text fields (http://gtkspell.sourceforge.net/)

This Project
--------------

* Project homepage: https://github.com/step-/yad

Upstream Project
--------------

Look here for versions supporting GTK+ 3 and for versions sporting new features.

* Project homepage: https://github.com/v1cont/yad
* Mailing list: http://groups.google.com/group/yad-common

Example usage & tutorials:

* http://smokey01.com/yad/
* http://www.murga-linux.com/puppy/viewtopic.php?t=97458
  "Yad Tips" (discussion forum topic containing 750+ posts)
* https://forums.bunsenlabs.org/viewtopic.php?id=1978
  "The great yad hacking thread" (discussion forum topic containing 230+ posts)
* https://sourceforge.net/p/yad-dialog/wiki/browse_pages/
* https://www.thelinuxrain.com/articles/the-buttons-of-yad
* https://github.com/jeteokeeffe/yad-examples



